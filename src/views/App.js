import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';
import FirstPage from './FirstPage';
import CreateWallet from './CreateWallet';
import ImportWallet from './ImportWallet';
import {
  homePage, firstPage, createWallet, importWallet,
} from '../constants/routes';
import 'Root/styles/base.less';
import '../../node_modules/rc-select/assets/index.less';

export default () => (
  <Switch>
    <Route path={'/dashboard'} exact component={Home} />
    <Route path={createWallet} exact component={CreateWallet} />
    <Route path={importWallet} exact component={ImportWallet} />
    <Route path={'/'} exact component={FirstPage} />
  </Switch>
);
