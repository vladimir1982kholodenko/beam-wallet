import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import LoginProcessLayout from 'Root/shared/components/Layouts/LoginProcessLayout';
import { Link } from 'react-router-dom';
import { createWallet } from 'Root/constants/routes';
import styles from './styles.less';

const ImportWallet = (props) => (
  <>
    <LoginProcessLayout
      title="Import Wallet"
      explain={(
        <div className={styles.padding}>
        if you must, please double-check the URL. it should say: www.something.com
        </div>
)}
    >
      <form>
        <div className="row justify-content-center mt-4 pt-4 mb-5 pb-5">
          <div className="col-10 px-xl-4 px-lg-0 px-md-0 px-sm-0 px-0">
            <div className="form-group">
              <label
                className="form-label-primary pb-1 mb-3"
                htmlFor="privateKey"
              >Private key
              </label>
              <input
                type="text"
                className="form-control form-control-primary"
                id="privateKey"
                aria-describedby="emailHelp"
                placeholder=""
              />
              <Link to="/dashboard">
                <button
                  type="button"
                  className={styles['load-wallet']}
                >
                    Load Wallet
                </button>
              </Link>
              <div className="text-center">
                <Link
                  to={createWallet}
                  className={classNames(styles['import-wallet'])}
                >
                  <span className="icon-arrow-left" />
                  <span>Create wallet</span>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </form>
    </LoginProcessLayout>
  </>
);

ImportWallet.propTypes = {

};

export default ImportWallet;
