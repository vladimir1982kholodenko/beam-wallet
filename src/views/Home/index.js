import React, { useState } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {
  TabContent, TabPane, Nav, NavItem, NavLink,
} from 'reactstrap';
import Header from 'Root/shared/components/Layouts/Header';
import AddressBar from 'Root/shared/components/AddressBar';
import TotalBeam from 'Root/shared/components/TotalBeam';
import DownloadWallet from 'Root/shared/components/DownloadWallet';
import Table from 'Root/shared/components/Table';
import { transactionData, utxData, addressData } from '../../constants/fake';
import styles from './styles.less';

const Home = (props) => {
  const [activeTab, toggleTab] = useState('1');
  const toggle = (tab) => {
    if (activeTab !== tab) {
      toggleTab(tab);
    }
  };

  const transactionHeader = [
    { name: 'Amount', width: '155px' },
    { name: 'TXID', width: '395px' },
    { name: 'Address', width: '185px' },
    { name: 'Date', width: '20%' },
  ];

  const utxoHeader = [
    { name: 'Amount', width: '258px' },
    { name: 'Maturity', width: '245px' },
    { name: 'Status', width: '265px' },
    { name: 'Type', width: '20%' },
  ];

  const addressHeader = [
    { name: 'Comment', width: '290px' },
    { name: 'Address', width: '190px' },
    { name: 'Status', width: '127px' },
    { name: 'Expiration date', width: '172px' },
    { name: 'Created', width: '137px' },
    { name: 'test', width: 'auto' },
  ];

  return (
    <>
      <Header />
      <div className="container-fluid base-padding">
        <div className="row">
          <div className="col-xl-8 col-lg-7 col-md-12 col-sm-12 col-12
         pr-xl-0 pr-lg-0"
          >
            <AddressBar />
          </div>
          <div className={classNames(styles.col,
            'col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12'
            + ' mt-xl-0 mt-lg-0 mt-md-3 mt-sm-3 mt-3 pl-xl-4')}
          >
            <TotalBeam />
          </div>
        </div>
        <div className="row mt-xl-4 mt-lg-4 mt-md-2 mt-sm-2 mt-2 pt-2">
          <div className="col-xl-8 col-lg-7 col-md-12 col-sm-12 col-12
         pr-xl-0 pr-lg-0"
          >
            <div className={classNames(styles.card, 'card card-primary pb-3')}>
              <Nav tabs className={classNames(styles.tabs, 'justify-content-center')}>
                <NavItem>
                  <NavLink
                    className={classNames({ active: activeTab === '1' })}
                    onClick={() => { toggle('1'); }}
                  >
                    Last Transactions
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classNames({ active: activeTab === '2' })}
                    onClick={() => { toggle('2'); }}
                  >
                    UTXO
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classNames({ active: activeTab === '3' })}
                    onClick={() => { toggle('3'); }}
                  >
                    Address
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                  <Table type="transaction" header={transactionHeader} body={transactionData} />
                </TabPane>
                <TabPane tabId="2">
                  <Table type="utxo" header={utxoHeader} body={utxData} />
                </TabPane>
                <TabPane tabId="3">
                  <Table type="address" header={addressHeader} body={addressData} />
                </TabPane>
              </TabContent>
            </div>
          </div>
          <div className={classNames(styles.col,
            'col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12'
            + ' mt-xl-0 mt-lg-0 mt-md-3 mt-sm-3 mt-3 pl-xl-4')}
          >
            <DownloadWallet />
          </div>
        </div>
      </div>
    </>
  );
};

Home.propTypes = {

};

export default Home;
