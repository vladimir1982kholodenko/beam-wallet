import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import LoginProcessLayout
  from 'Root/shared/components/Layouts/LoginProcessLayout';
import CopyText from 'Root/shared/components/CopyText';
import { importWallet } from 'Root/constants/routes';
import styles from './styles.less';

const CreateWallet = () => (
  <>
    <LoginProcessLayout
      title="Create wallet"
      explain="Download your wallet file and save the private key"
    >
      <form>
        <div className="row justify-content-center mt-4 pt-4 pr-3">
          <div className="col-11 offset-1 pl-xl-4 pl-lg-0 pl-md-0 pl-sm-0 pl-0 pr-5">
            <div className="form-group">
              <label
                className="form-label-primary pb-1 mb-3"
                htmlFor="privateKey"
              >Private key
              </label>
              <div className="d-flex">
                <input
                  type="text"
                  className="form-control form-control-primary"
                  id="privateKey"
                  aria-describedby="emailHelp"
                  placeholder="Enter your private key"
                />
                <div className="position-relative pl-3">
                  <span className="child-center-ver">
                    <CopyText
                      text="tttttttttttttt"
                      class="copy-class-blue"
                    />
                  </span>
                </div>
              </div>
            </div>
            <div className="form-group pt-3">
              <label
                className="form-label-primary pb-1 mb-3"
                htmlFor="address"
              >Your address
              </label>
              <div className="d-flex">
                <input
                  type="text"
                  className="form-control form-control-primary"
                  id="address"
                  aria-describedby="emailHelp"
                  placeholder="Enter your address"
                />
                <div className="position-relative pl-3">
                  <span className="child-center-ver">
                    <CopyText
                      text="tttttttttttttt"
                      class="copy-class-blue"
                    />
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row justify-content-center
        mt-4 mb-5 pb-5
        pl-xl-0 pl-lg-0 pl-md-3 pl-sm-3 pl-3"
        >
          <div className="col-xl-3 col-lg-3 col-md-11 col-sm-11 col-11
          pl-xl-4 pl-lg-0 pl-md-0 pl-sm-0 pl-0 position-relative"
          >
            <Link
              to={importWallet}
              className={classNames(styles['import-wallet'],
                'child-center-ver child-center-ver-lg')}
            >
              <span className="icon-arrow-left" />
              <span>Import wallet</span>
            </Link>
          </div>
          <div className="col-xl-7 col-lg-7 col-md-11 col-sm-11 col-11
          text-xl-right text-lg-right text-md-left text-sm-left text-left
          mt-xl-auto mt-lg-auto mt-md-3 mt-sm-3 mt-3
          pl-xl-3 pl-lg-0 pl-md-0 pl-sm-0 pl-0"
          >
            <button type="button" className={styles.download}>Download
            </button>
            <Link to="/dashboard">
              <button type="button" className={styles.next}>
                <span>Next</span>
                <span className="icon-arrow-right" />
              </button>
            </Link>
          </div>
        </div>
      </form>
    </LoginProcessLayout>

  </>
);

CreateWallet.propTypes = {};

export default CreateWallet;
