import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import LoginProcessLayout from 'Root/shared/components/Layouts/LoginProcessLayout';
import importIcon from 'Root/assets/images/import.png';
import plusIcon from 'Root/assets/images/plus.png';
import { importWallet, createWallet } from 'Root/constants/routes';
import styles from './styles.less';

const FirstPage = () => (
  <>
    <LoginProcessLayout
      title="How would you like to access your wallet?"
      explain="Choose one way to access your wallet"
    >
      <div className="row mt-4 pt-4 justify-content-center">
        <div className="col-xl-6 col-lg-6 col-md-8 col-sm-10 col-12
         pr-xl-4 pr-lg-4 pr-md-3 pr-sm-3 pr-3"
        >
          <Link to={createWallet} className={classNames(styles.card, 'card card-secondary')}>
            <img src={plusIcon} width="40px" height="40px" alt="plus" className="d-block mx-auto" />
            <h4 className={styles.title}>Create Wallet</h4>
            <p className={styles.text}>Connect & sign via your hardware wallet</p>
          </Link>
        </div>
        <div className="col-xl-6 col-lg-6 col-md-8 col-sm-10 col-12
        pl-xl-4 pl-lg-4 pl-md-3 pl-sm-3 pl-3 mt-xl-0 mt-lg-0 mt-md-3 mt-sm-3 mt-3"
        >
          <Link to={importWallet} className={classNames(styles.card, 'card card-secondary')}>
            <img src={importIcon} width="40px" height="40px" alt="plus" className="d-block mx-auto" />
            <h4 className={styles.title}>Import Wallet</h4>
            <p className={styles.text}>Connect & sign via your hardware wallet</p>
          </Link>
        </div>
      </div>
    </LoginProcessLayout>
  </>
);

FirstPage.propTypes = {

};

export default FirstPage;
