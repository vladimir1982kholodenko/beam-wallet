export const homePage = '/';
export const firstPage = '/first-page';
export const createWallet = '/create-wallet';
export const importWallet = '/import-wallet';
