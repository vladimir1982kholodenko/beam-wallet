export const transactionData = [
  {
    0: [true, '0.452'],
    1: '37hhsdbkd73379hvvggmggcfhklnhgcffhiougvbbjk',
    2: '9Bjsi2…8d1eks',
    3: ['10 june 2019', '08:55:44'],
  },
  {
    0: [true, '0.452'],
    1: '37hhsdbkd73379hvvggmggcfhklnhgcffhiougvbbjk',
    2: '9Bjsi2…8d1eks',
    3: ['10 june 2019', '08:55:44'],
  },
  {
    0: [true, '0.452'],
    1: '37hhsdbkd73379hvvggmggcfhklnhgcffhiougvbbjk',
    2: '9Bjsi2…8d1eks',
    3: ['10 june 2019', '08:55:44'],
  },
  {
    0: [false, '0.156'],
    1: '37hhsdbkd73379hvvggmggcfhklnhgcffhiougvbbjk',
    2: '9Bjsi2…8d1eks',
    3: ['10 june 2019', '08:55:44'],
  },
  {
    0: [true, '0.452'],
    1: '37hhsdbkd73379hvvggmggcfhklnhgcffhiougvbbjk',
    2: '9Bjsi2…8d1eks',
    3: ['10 june 2019', '08:55:44'],
  },
  {
    0: [false, '0.156'],
    1: '37hhsdbkd73379hvvggmggcfhklnhgcffhiougvbbjk',
    2: '9Bjsi2…8d1eks',
    3: ['10 june 2019', '08:55:44'],
  },
  {
    0: [true, '0.452'],
    1: '37hhsdbkd73379hvvggmggcfhklnhgcffhiougvbbjk',
    2: '9Bjsi2…8d1eks',
    3: ['10 june 2019', '08:55:44'],
  },
  {
    0: [true, '0.452'],
    1: '37hhsdbkd73379hvvggmggcfhklnhgcffhiougvbbjk',
    2: '9Bjsi2…8d1eks',
    3: ['10 june 2019', '08:55:44'],
  },
];

export const utxData = [
  {
    0: '8.681475',
    1: '347284',
    2: 'Available',
    3: 'Change',
  },
  {
    0: '0.0000009',
    1: '344316',
    2: 'Available',
    3: 'Regular',
  },
  {
    0: '9.681575',
    1: '344309',
    2: 'Spent',
    3: 'Change',
  },
  {
    0: '0.008989',
    1: '344308',
    2: 'Available',
    3: 'Change',
  },
  {
    0: '9.781675',
    1: '344286',
    2: 'Spent',
    3: 'Change',
  },
  {
    0: '0.00999',
    1: '344283',
    2: 'Spent',
    3: 'Change',
  },
];

export const addressData = [
  {
    0: 'Somthing for comment',
    1: 'a4bfe43a20c20d0ca47013d287a7c05b021a93763d41fe57e74ccf14e5341feb77',
    2: 'Never',
    3: ['10 june 2019', '08:55:44'],
    4: ['10 june 2019', '13:30 UTC'],
  },
  {
    0: 'Somthing for comment',
    1: '22a65b49be11472662a432e61bc34133bd8a3b7be509c8124fe1bd6e48e5f441789',
    2: 'Never',
    3: ['10 june 2019', '08:55:44'],
    4: ['10 june 2019', '12:30 UTC'],
  },
  {
    0: 'Somthing for comment',
    1: '1b00bf6b74d7045cdb0fc074afb9c8d36be5329ce9fb2bd512df8cdeb1ce1ff2bc2',
    2: 'Never',
    3: ['10 june 2019', '08:55:44'],
    4: ['10 june 2019', '09:30 UTC'],
  },
  {
    0: 'Somthing for comment',
    1: '25a98de9b3e1e6dfa5a7d971fac196ab065d0a3528cfe433850a8db189310d695b2',
    2: 'Never',
    3: ['10 june 2019', '08:55:44'],
    4: ['10 june 2019', '14:30 UTC'],
  },
  {
    0: 'Somthing for comment',
    1: '810f79d037ecf1a48153640d7fb416c709c243138259009e89e2d9e601e97afb27',
    2: 'Never',
    3: ['10 june 2019', '08:55:44'],
    4: ['10 june 2019', '15:30 UTC'],
  },
];
