import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import B from 'Root/assets/images/B.png';
import { Collapse } from 'reactstrap';
import styles from './styles.less';

const TransactionRow = ({ td, header }) => {
  const [displayBlock, toggleBlock] = useState(false);
  return (
    <>
      <div
        className="div-tr"
        onClick={() => {
          toggleBlock(!displayBlock);
        }}
      >
        <td className={styles.amount} style={{ width: `${header[0].width}` }}>
          <div className="d-flex">
            <div className={classNames(styles.arrow, 'position-relative mr-2')}>
              {!td[Object.keys(td)[0]][0] && (<span className="icon-long-arrow-up child-center-hor-ver" />)}
              {td[Object.keys(td)[0]][0] && (<span className="icon-long-arrow-down child-center-hor-ver" />)}
            </div>
            <span>{td[Object.keys(td)[0]][1]}</span>
            <span className="pl-2">
              <img src={B} width="10px" height="13px" alt="b" />
            </span>
          </div>
        </td>
        <td className={styles.address} style={{ width: `${header[1].width}` }}>{td[Object.keys(td)[1]]}</td>
        <td style={{ width: `${header[2].width}` }}>{td[Object.keys(td)[2]]}</td>
        <td className={styles.date} style={{ width: '20%' }}>
          {/* <p className="mb-0">{td[Object.keys(td)[3]][0]}</p>
          <p className="mb-0">{td[Object.keys(td)[3]][1]}</p> */}
          <p className="mb-0">2 minutes ago</p>
        </td>
      </div>
      <Collapse isOpen={displayBlock}>
        <div className={styles.info}>
          <h2 className={styles.title}>General transaction info</h2>
          <div className="container-fluid pt-2">
            <div className="row">
              <div className="col-2 pl-0">
                <p className={styles['title-info']}>Send address:</p>
              </div>
              <div className="col-10 pl-5">
                <p className={styles['title-text']}>
                  3b0353a850a437697b03fe80a8798d1e6f9f4916cb00b6fefead63401f1ad9868a4
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-2 pl-0">
                <p className={styles['title-info']}>Recive address:</p>
              </div>
              <div className="col-10 pl-5">
                <p className={styles['title-text']}>
                  3b0353a850a437697b03fe80a8798d1e6f9f4916cb00b6fefead63401f1ad9868a4
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-2 pl-0">
                <p className={styles['title-info']}>Transaction fee:</p>
              </div>
              <div className="col-10 pl-5">
                <p className={styles['title-text']}>
                  0.000013
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-2 pl-0">
                <p className={styles['title-info']}>Comment:</p>
              </div>
              <div className="col-10 pl-5">
                <p className={styles['title-text']}>
                  Something…
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-2 pl-0">
                <p className={styles['title-info']}>Transaction ID:</p>
              </div>
              <div className="col-10 pl-5">
                <p className={styles['title-text']}>
                  37hhsdbkd73379hvvggmggcfhklnhgcffhiougvbbjk
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-2 pl-0">
                <p className={styles['title-info']}>Kernel ID:</p>
              </div>
              <div className="col-10 pl-5">
                <p className={styles['title-text']}>
                  37hhsdbkd73379hvvggmggcfhklnhgcffhiougvbbjkklnhgcffhiougvbbjk
                </p>
              </div>
            </div>
          </div>
        </div>
      </Collapse>
    </>
  );
};

TransactionRow.propTypes = {
  td: PropTypes.object.isRequired,
  header: PropTypes.array.isRequired,
};

export default TransactionRow;
