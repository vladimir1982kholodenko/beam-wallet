import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import B from 'Root/assets/images/B.png';
import styles from './styles.less';


const UtxoRow = ({ header, td }) => (
  <>
    <td className={styles.amount} style={{ width: `${header[0].width}` }}>
      <span>{td[Object.keys(td)[0]]}</span>
      <span className="pl-2">
        <img src={B} width="10px" height="13px" alt="b" />
      </span>
    </td>
    <td className={styles.maturity} style={{ width: `${header[1].width}` }}>{td[Object.keys(td)[1]]}</td>
    <td
      className={classNames(styles.status,
        td[Object.keys(td)[2]].toLowerCase() === 'available'
          ? styles['status-blue'] : styles['status-red'])}
      style={{ width: `${header[2].width}` }}
    >
      {td[Object.keys(td)[2]]}
    </td>
    <td className={styles.type} style={{ width: `${header[3].width}` }}>{td[Object.keys(td)[3]]}</td>
  </>
);

UtxoRow.propTypes = {
  td: PropTypes.object.isRequired,
  header: PropTypes.array.isRequired,
};

export default UtxoRow;
