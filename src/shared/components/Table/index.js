import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import TransactionRow from './TransactionRow';
import UtxoRow from './UtxoRow';
import AddressRow from './AddressRow';
import styles from './styles.less';

const Table = ({ header, body, type }) => (
  <>
    <div className={styles.scroll} style={{ overflowX: 'hidden' }}>
      <table className="table primary-type-table mb-0" style={{ width: 'calc(100% - 6px)' }}>
        <thead>
          <tr>
            {header.map((th, index) => (
              <th
                key={index}
                scope="col"
                width={th.width}
                className={classNames(
                  (type === 'address') && (index === header.length - 1)
                    ? 'invisible' : '',
                )}
              >{th.name}
              </th>
            ))}
          </tr>
        </thead>
      </table>
      <div className="table-scroll-vertical">
        <table className="table primary-type-table mb-0">
          <tbody>
            {type === 'transaction' && body.map((td, index) => (
              <tr key={index}>
                <TransactionRow td={td} header={header} />
              </tr>
            ))}
            {type === 'utxo' && body.map((td, index) => (
              <tr key={index}>
                <UtxoRow td={td} header={header} />
              </tr>
            ))}
            {type === 'address' && body.map((td, index) => (
              <tr key={index}>
                <AddressRow td={td} header={header} />
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  </>
);

Table.propTypes = {
  header: PropTypes.array.isRequired,
  body: PropTypes.array.isRequired,
  type: PropTypes.string.isRequired,
};

export default Table;
