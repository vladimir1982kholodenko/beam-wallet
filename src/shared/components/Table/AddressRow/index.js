import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
  Dropdown, DropdownMenu, DropdownToggle, DropdownItem,
} from 'reactstrap';
import EditAddressModal from 'Root/shared/components/Modals/EditAddressModal';
import QrCodeModal from 'Root/shared/components/Modals/QrCodeModal';
import styles from './styles.less';


const AddressRow = ({ header, td }) => {
  const [displayDropdown, toggleDropDown] = useState(false);
  const [editModal, toggleEditModal] = useState(false);
  const [qrModal, toggleQrModal] = useState(false);
  const address = td[Object.keys(td)[1]];
  return (
    <>
      <td className={styles.comment} style={{ width: `${header[0].width}` }}>{td[Object.keys(td)[0]]}</td>
      <td className={styles.address} style={{ width: `${header[1].width}` }}>
        {address.substr(0, 6).concat('...').concat(address.substr(address.length - 6, address.length))}
      </td>
      <td className={styles.status} style={{ width: `${header[2].width}` }}>Active</td>
      <td className={styles.expiration} style={{ width: `${header[3].width}` }}>
        <p className="mb-0">{td[Object.keys(td)[3]][0]}</p>
        <p className="mb-0">{td[Object.keys(td)[3]][1]}</p>
      </td>
      <td className={styles.created} style={{ width: `${header[4].width}` }}>
        <p className="mb-0">{td[Object.keys(td)[4]][0]}</p>
        <p className="mb-0">{td[Object.keys(td)[4]][1]}</p>
      </td>
      <td className={styles.maturity} style={{ width: `${header[5].width}` }}>
        <Dropdown
          direction="left"
          isOpen={displayDropdown}
          toggle={() => { toggleDropDown(!displayDropdown); }}
        >
          <DropdownToggle className={classNames(styles.btn, 'btn')}>
            <span className="icon-expand" />
          </DropdownToggle>
          <DropdownMenu className={styles.drop}>
            <DropdownItem onClick={() => toggleQrModal(!qrModal)}>
              <span className="icon-qr-code pr-3" />
              Show QR cod
            </DropdownItem>
            <DropdownItem onClick={() => toggleEditModal(!editModal)}>
              <span className="icon-edit pr-3" />
              Edit Address
            </DropdownItem>
            <DropdownItem><span className="icon-recycle-bin pr-3" />Delete Address</DropdownItem>
          </DropdownMenu>
        </Dropdown>
      </td>
      <EditAddressModal toggle={toggleEditModal} modal={editModal} />
      <QrCodeModal toggle={toggleQrModal} modal={qrModal} />
    </>
  );
};

AddressRow.propTypes = {
  td: PropTypes.object.isRequired,
  header: PropTypes.array.isRequired,
};

export default AddressRow;
