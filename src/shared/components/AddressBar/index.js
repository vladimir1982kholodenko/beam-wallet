import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CopyText from 'Root/shared/components/CopyText';
import QrCodesListModal from 'Root/shared/components/Modals/QrCodesListModal';
import SendModal from 'Root/shared/components/Modals/SendModal';
import QrCodeModal from 'Root/shared/components/Modals/QrCodeModal';
import AddAddressModal from 'Root/shared/components/Modals/AddAddressModal';
import styles from './styles.less';


const AddressBar = (props) => {
  const [qrModal, toggleQrModal] = useState(false);
  const [qrListModal, toggleQrListModal] = useState(false);
  const [sendModal, toggleSendModal] = useState(false);
  const [addModal, toggleAddModal] = useState(false);
  return (
    <>
      <div className={classNames(styles.card, 'card card-primary')}>
        <h2 className={styles['address-title']}>Address:</h2>
        <h2 className={styles['address-part']}>
          <span className={styles.address}>
          3b0353a850a437697b03fe80a8798d1e6f9f4916cb00b6fefead63401f1ad9868a4
          </span>
          <CopyText
            class="copy-class-gray"
            text="3b0353a850a437697b03fe80a8798d1e6f9f4916cb00b6fefead63401f1ad9868a4"
          />
          <button type="button" onClick={() => toggleQrModal(!qrModal)} className="icon-qr-code btn p-0" />
          <button
            style={{ display: 'none' }}
            type="button"
            onClick={() => toggleAddModal(!addModal)}
            className={styles.add}
          >
            <span className="icon-plus pr-2" />
            <span>Add NewAddress</span>
          </button>
        </h2>
        <div className="d-flex mt-3">
          <button
            type="button"
            className={classNames(styles.receive)}
            onClick={() => toggleQrListModal(!qrListModal)}
          >
            <span>Receive</span>
            <span className={classNames('icon-small-arrow-down pl-2')} />
          </button>

          <button
            type="button"
            className={classNames(styles.send, '')}
            onClick={() => toggleSendModal(!sendModal)}
          >
            <span>Send</span>
            <span className={classNames('icon-send-message pl-3 mt-1')} />
          </button>
        </div>
      </div>
      <QrCodesListModal toggle={toggleQrListModal} modal={qrListModal} />
      <QrCodeModal toggle={toggleQrModal} modal={qrModal} />
      <SendModal toggle={toggleSendModal} modal={sendModal} />
      <AddAddressModal toggle={toggleAddModal} modal={addModal} />
    </>
  );
};

AddressBar.propTypes = {

};

export default AddressBar;
