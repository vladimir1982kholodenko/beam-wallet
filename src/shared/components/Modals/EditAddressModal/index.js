import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Modal } from 'reactstrap';
import Select, { Option } from 'rc-select';
import Switch from 'react-switch';
import styles from './styles.less';

const EditAddressModal = ({ modal, toggle }) => {
  const onChange = (value) => {
    console.log(`selected ${value}`);
  };
  const [checked, toggleCheck] = useState(false);
  return (
    <>
      <Modal isOpen={modal} size="lg" toggle={() => toggle()} className={styles.transaction}>
        <div className="row">
          <div className="col-12">
            <h4>
              <span className={styles.title}>Edit address</span>
              <button
                type="button"
                className="btn p-0 icon-remove float-right"
                onClick={() => toggle()}
              />
            </h4>
          </div>
        </div>
        <div className={styles.hr} />
        <div className="row mt-4">
          <div className="col-12">
            <h5 className={styles.address}>Address ID</h5>
            <p className={styles['address-text']}>
              3b0353a850a437697b03fe80a8798d1e6f9f4916cb00b6fefead63401f1ad9868a4
            </p>
          </div>
        </div>
        <div className="row mt-3">
          <div className="col-12">
            <label className="form-label-secondary pb-1">Expire</label>
            <Select
              placeholder="placeholder"
              defaultValue="Within 24 hours"
              style={{ width: '100%', paddingTop: '9px', paddingBottom: '11px' }}
              animation="slide-up"
              showSearch={false}
              onChange={onChange}
            >
              <Option value="1">Within 24 hours</Option>
              <Option value="2">Within 48 hours</Option>
              <Option value="3">Within 72 hours</Option>
            </Select>
          </div>
        </div>
        <div className={classNames('row mt-4 pt-3 justify-content-center')}>
          <div className={classNames('col-11 px-2')}>
            <div className={classNames(styles.switch, 'row')}>
              <div className="col-8">
                <label className="form-label-secondary">Expire address now</label>
              </div>
              <div className="col-4 text-right">
                <Switch
                  onChange={toggleCheck}
                  checked={checked}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  offColor="#52718a"
                  onColor="#52718a"
                  offHandleColor="#7a98b1"
                  onHandleColor="#7a98b1"
                  height={30}
                  width={60}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-4">
          <div className="col-12">
            <div className="form-group">
              <label htmlFor="comment" className="form-label-secondary pb-1">Comment</label>
              <textarea
                className="form-control text-area-primary"
                placeholder="Write your comment here"
                id="comment"
                rows="5"
              />
            </div>
          </div>
        </div>
        <div className={styles['hr-bottom']} />
        <div className="row mt-4 pt-2">
          <div className="col-12">
            <button type="button" className={classNames(styles.btn)}>Save</button>
          </div>
        </div>
      </Modal>
    </>
  );
};

EditAddressModal.defaultProps = {
  modal: false,
};

EditAddressModal.propTypes = {
  modal: PropTypes.bool,
  toggle: PropTypes.func.isRequired,
};

export default EditAddressModal;
