import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Modal } from 'reactstrap';
import QrCode from 'Root/shared/components/QrCode';
import styles from './styles.less';

const QrCodesListModal = ({ modal, toggle }) => (
  <>
    <Modal isOpen={modal} size="lg" toggle={() => toggle()} className={styles.qr}>
      <div className="row mb-4 pb-3">
        <div className="col-12">
          <button
            type="button"
            className="btn p-0 icon-remove float-right"
            onClick={() => toggle()}
          />
        </div>
      </div>
      <div className={classNames(styles.scroll)} dir="rtl">
        {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map((index) => (
          <div key={index} dir="ltr">
            <div className="row">
              <div className="col-12">
                <QrCode />
              </div>
            </div>
            <div className={classNames(index === 11 ? 'my-5 pb-2' : '')} />
            <div className={classNames('row mx-1', index === 11 ? 'd-none' : '')}>
              <div className="col-12 px-0">
                <hr className={styles.hr} />
              </div>
            </div>
          </div>
        ))}
      </div>
    </Modal>
  </>
);

QrCodesListModal.defaultProps = {
  modal: false,
};

QrCodesListModal.propTypes = {
  modal: PropTypes.bool,
  toggle: PropTypes.func.isRequired,
};

export default QrCodesListModal;
