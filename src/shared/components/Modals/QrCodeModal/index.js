import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'reactstrap';
import QrCode from 'Root/shared/components/QrCode';
import styles from './styles.less';

const QrCodeModal = ({ modal, toggle }) => (
  <>
    <Modal isOpen={modal} size="lg" toggle={() => toggle()} className={styles.qr}>
      <div className="row">
        <div className="col-12">
          <button
            type="button"
            className="btn p-0 icon-remove float-right"
            onClick={() => toggle()}
          />
        </div>
      </div>
      <div className="row mt-4 pt-3">
        <div className="col-12">
          <QrCode />
        </div>
      </div>
    </Modal>
  </>
);

QrCodeModal.defaultProps = {
  modal: false,
};

QrCodeModal.propTypes = {
  modal: PropTypes.bool,
  toggle: PropTypes.func.isRequired,
};

export default QrCodeModal;
