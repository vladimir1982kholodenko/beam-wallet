import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Modal } from 'reactstrap';
import B from 'Root/assets/images/B.png';
import styles from './styles.less';
import SuccessfulSendingModal from '../SuccessfulSendingModal';

const ConfirmTransactionModal = ({ modal, toggle }) => {
  const [successModal, toggleSuccessModal] = useState(false);
  return (
    <>
      <Modal isOpen={modal} size="lg" toggle={() => toggle()} className={styles.confirm}>
        <div className="row">
          <div className="col-12">
            <h4>
              <span className={styles.title}>
                <span className="icon-check-circle pr-2 pt-1" />
              Confirm transaction
              </span>
              <button
                type="button"
                className="btn p-0 icon-remove float-right"
                onClick={() => toggle()}
              />
            </h4>
          </div>
        </div>
        <div className={styles.hr} />
        <div className="row mt-4">
          <div className="col-3">
            <p className={styles.from}>
              <span>From</span>
              <span className="icon-both-directions float-right" />
            </p>
          </div>
          <div className="col-9">
            <p className={styles['from-address']}>
            3b0353a857697b03fe80a8798d1…fefead63401f1ad9868a4
            </p>
          </div>
        </div>
        <div className="row mt-4">
          <div className="col-3">
            <p className={styles.from}>
              <span>To</span>
              <span className="icon-both-directions float-right" />
            </p>
          </div>
          <div className="col-9">
            <p className={styles['from-address']}>
            3b0353a857697b03fe80a8798d1…fefead63401f1ad9868a4
            </p>
          </div>
        </div>
        <div className="row mt-4 mb-3">
          <div className="col-12">
            <div className={styles.box}>
              <div className="row">
                <div className="col pl-0">
                  <p className={styles['box-label']}>You’ll Send</p>
                </div>
                <div className="col pr-0">
                  <p className={styles.equal}>
                    <span>10{' '}<img src={B} width="10px" height="15px" alt="b" /></span>
                    <span className="icon-approximately-equal px-3" />
                    <span>$12</span>
                  </p>
                </div>
              </div>
              <div className="row mt-4">
                <div className="col pl-0">
                  <p className={styles['box-label']}>You’Transaction Fee</p>
                </div>
                <div className="col pr-0">
                  <p className={styles.equal}>
                    <span>0{' '}<img src={B} width="10px" height="15px" alt="b" /></span>
                    <span className="icon-approximately-equal px-3" />
                    <span>$0</span>
                  </p>
                </div>
              </div>
              <div className="row pt-1">
                <div className="col-12 px-0">
                  <hr className={styles.separator} />
                </div>
              </div>
              <div className="row">
                <div className="col pl-0">
                  <p className={styles['box-label']}>Total</p>
                </div>
                <div className="col pr-0">
                  <p className={styles.equal}>
                    <span>10{' '}<img src={B} width="10px" height="15px" alt="b" /></span>
                    <span className="icon-approximately-equal px-3" />
                    <span>$12</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.hr} />
        <div className="row mt-4 pt-2">
          <div className="col-12">
            <button
              type="button"
              className={classNames(styles.btn)}
              onClick={() => { toggleSuccessModal(!successModal); toggle(); }}
            >
              <span>Confirm</span>
            </button>
          </div>
        </div>
      </Modal>
      <SuccessfulSendingModal toggle={toggleSuccessModal} modal={successModal} />
    </>
  );
};

ConfirmTransactionModal.defaultProps = {
  modal: false,
};

ConfirmTransactionModal.propTypes = {
  modal: PropTypes.bool,
  toggle: PropTypes.func.isRequired,
};

export default ConfirmTransactionModal;
