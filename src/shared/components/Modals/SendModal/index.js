import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'reactstrap';
import InputGroup from 'Root/shared/components/InputGroup';
import ConfirmTransactionModal from 'Root/shared/components/Modals/ConfirmTransactionModal';
import classNames from 'classnames';
import styles from './styles.less';

const SendModal = ({ modal, toggle }) => {
  const [confirmModal, toggleConfirmModal] = useState(false);

  return (
    <>
      <Modal isOpen={modal} size="lg" toggle={() => toggle()} className={styles.send}>
        <div className="row">
          <div className="col-12">
            <h4>
              <span className={styles.title}>
                <span className="icon-send-message pr-2 pt-1" />
              Send Assets
              </span>
              <button
                type="button"
                className="btn p-0 icon-remove float-right"
                onClick={() => toggle()}
              />
            </h4>
          </div>
        </div>
        <div className={styles.hr} />
        <div className="row mt-4 pt-1">
          <div className="col-12">
            <div className="form-group">
              <label
                className="form-label-secondary pb-1"
                htmlFor="transfer"
              >Transfer to address
              </label>
              <input
                type="text"
                className="form-control form-control-secondary"
                id="transfer"
                placeholder="Your wallet address"
              />
            </div>
            <div className="form-group mt-4">
              <label
                className="form-label-secondary pb-1"
                htmlFor="transfer"
              >Amount
              </label>
              <InputGroup>
                <input
                  type="text"
                  id="transfer"
                  className="form-control primary-input-group"
                  placeholder="How much to transfer"
                />
              </InputGroup>
            </div>
            <div className="form-group mt-4">
              <label
                className="form-label-secondary pb-1"
                htmlFor="fee"
              >Fee
              </label>
              <InputGroup>
                <input
                  type="text"
                  id="fee"
                  className="form-control primary-input-group"
                  placeholder="Enter your fee"
                />
              </InputGroup>
            </div>
            <div className="form-group mt-4">
              <label htmlFor="comment" className="form-label-secondary pb-1">Comment</label>
              <textarea
                className="form-control text-area-primary"
                placeholder="Write your comment here"
                id="comment"
                rows="5"
              />
            </div>
          </div>
        </div>
        <div className={styles.hr} />
        <div className="row mt-4 pt-2">
          <div className="col-12">
            <button
              type="button"
              className={classNames(styles.btn)}
              onClick={() => { toggleConfirmModal(!confirmModal); toggle(); }}
            >
              <span>Send</span>
              <span className="icon-send-message pl-3" />
            </button>
          </div>
        </div>
      </Modal>
      <ConfirmTransactionModal toggle={toggleConfirmModal} modal={confirmModal} />
    </>
  );
};

SendModal.defaultProps = {
  modal: false,
};

SendModal.propTypes = {
  modal: PropTypes.bool,
  toggle: PropTypes.func.isRequired,
};

export default SendModal;
