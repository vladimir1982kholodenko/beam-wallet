import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Modal } from 'reactstrap';
import styles from './styles.less';

const AddAddressModal = ({ modal, toggle }) => (
  <>
    <Modal isOpen={modal} size="lg" toggle={() => toggle()} className={styles.add}>
      <div className="row">
        <div className="col-12">
          <h4>
            <span className={styles.title}>
              <span className="icon-clipboard-notes pr-2 pt-1" />
              Add new address
            </span>
            <button
              type="button"
              className="btn p-0 icon-remove float-right"
              onClick={() => toggle()}
            />
          </h4>
        </div>
      </div>
      <div className={styles.hr} />
      <div className="row mt-4 pt-1">
        <div className="col-12">
          <div className="form-group">
            <label
              className="form-label-secondary pb-1"
              htmlFor="transfer"
            >Transfer to address
            </label>
            <input
              type="text"
              className="form-control form-control-secondary"
              id="transfer"
              placeholder="Your wallet address"
            />
          </div>
          <div className="form-group mt-4">
            <label htmlFor="comment" className="form-label-secondary pb-1">Comment</label>
            <textarea
              className="form-control text-area-primary"
              placeholder="Write your comment here"
              id="comment"
              rows="5"
            />
          </div>
        </div>
      </div>
      <div className={styles.hr} />
      <div className="row mt-4 pt-2">
        <div className="col-12">
          <button type="button" className={classNames(styles.btn)}>
            <span>Create</span>
          </button>
        </div>
      </div>
    </Modal>
  </>
);

AddAddressModal.defaultProps = {
  modal: false,
};

AddAddressModal.propTypes = {
  modal: PropTypes.bool,
  toggle: PropTypes.func.isRequired,
};

export default AddAddressModal;
