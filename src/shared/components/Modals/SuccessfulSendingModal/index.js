import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Modal } from 'reactstrap';
import CopyText from 'Root/shared/components/CopyText';
import styles from './styles.less';

const SuccessfulSendingModal = ({ modal, toggle }) => {
  const [copy, changeCopy] = useState(false);
  return (
    <>
      <Modal isOpen={modal} size="lg" toggle={() => toggle()} className={styles.success}>
        <div className="row">
          <div className="col-12">
            <button
              type="button"
              className="btn p-0 icon-remove float-right"
              onClick={() => toggle()}
            />
          </div>
        </div>
        <div className="row justify-content-center my-4">
          <div className="col-xl-7 col-lg-7 col-md-7 col-sm-8 col-9 text-center px-0">
            <span className={classNames(styles.icon, 'icon-check-circle')} />
            <h2 className={styles.title}>Transaction Sent</h2>
            <p className={styles.address}>
            3b0353a850a437697b03fe80a8798d1e6f9f4916cb00b6fefead63401f1ad9868a4
            </p>
            <CopyText text="3b0353a850a437697b03fe80a8798d1e6f9f4916cb00b6fefead63401f1ad9868a4">
              <button
                className={styles.btn}
                onClick={() => {
                  changeCopy(!copy);
                  setTimeout(() => { changeCopy(false); }, 700);
                }}
              >
                <span className="icon-copy pr-2" />
                {copy && <span>Copied!</span>}
                {!copy && <span>Copy address</span>}
              </button>
            </CopyText>
          </div>
        </div>
      </Modal>
    </>
  );
};
SuccessfulSendingModal.defaultProps = {
  modal: false,
};

SuccessfulSendingModal.propTypes = {
  modal: PropTypes.bool,
  toggle: PropTypes.func.isRequired,
};

export default SuccessfulSendingModal;
