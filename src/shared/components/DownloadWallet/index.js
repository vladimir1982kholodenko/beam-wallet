import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './styles.less';

const DownloadWallet = () => (
  <>
    <div className={classNames(styles.card, 'card card-primary h-auto')}>
      <div className="row">
        <div className="col-12 text-center">
          <h2 className={styles.title}>Download Wallet</h2>
          <p className={styles.text}>Click on the download button below to download your wallet</p>
          <button type="button" className={styles.btn}>Download</button>
        </div>
      </div>
    </div>
  </>
);

DownloadWallet.propTypes = {

};

export default DownloadWallet;
