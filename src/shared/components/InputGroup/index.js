import React from 'react';
import B from 'Root/assets/images/B.png';

const InputGroup = (props) => (
  <div className="input-group primary-input-group-box mb-2">
    {props.children}
    <div className="input-group-prepend">
      <div className="input-group-img position-relative">
        <img
          src={B}
          width="12px"
          height="18px"
          alt="b"
          className="child-center-hor-ver"
        />
      </div>
    </div>
  </div>
);

export default InputGroup;
