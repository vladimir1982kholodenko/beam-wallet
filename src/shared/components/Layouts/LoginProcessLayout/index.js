import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import background from 'Root/assets/images/background.png';
import logo from 'Root/assets/images/logo.png';
import styles from './styles.less';

const LoginProcessLayout = (props) => (
  <>
    <div
      className="container-fluid
      pb-5 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5"
      style={{ minHeight: '100vh' }}
    >
      <div className="row">
        <div className="col-12 px-0">
          <img src={background} alt="back" className={styles.back} />
        </div>
      </div>
      <div className={classNames(styles['content-box'], 'row justify-content-center')}>
        <div className="col-xl-6 col-lg-8 col-md-9 col-sm-10 col-11 px-2">
          <img
            src={logo}
            width="70px"
            height="47.6px"
            alt="beam"
            className="d-block mx-auto"
          />
          <h1 className={styles.title}>{props.title}</h1>
          <p className={styles.text}>{props.explain}</p>
          {props.children}
        </div>
      </div>
    </div>
  </>
);

LoginProcessLayout.propTypes = {
  title: PropTypes.string.isRequired,
  explain: PropTypes.string.isRequired,
};

export default LoginProcessLayout;
