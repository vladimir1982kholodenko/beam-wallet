import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import logout from 'Root/assets/images/logout.png';
import styles from './styles.less';

const Header = (props) => (
  <>
    <div className={classNames(styles.header, 'container-fluid header-padding')}>
      <div className="row">
        <div className="col-12">
          <ul className="nav nav-pills nav-fill">
            <li className="nav-item text-left">
              <button type="button" className={classNames(styles.refresh, 'btn nav-link pt-2 pl-0')}>
                <span className="icon-refresh" />
              </button>
            </li>
            <li className="nav-item">
              <h1 className={classNames(styles.title, 'nav-link mb-0 pt-2 mt-1')}>
                <span className={styles.block}>Block:</span>
                <span className={styles['block-amount']}>379508</span>
              </h1>
            </li>
            <li className="nav-item text-right">
              <Link to="/">
                <button
                  type="button"
                  className={classNames(styles['shout-down'],
                  'btn nav-link pt-2 pr-0 float-right')}
                >
                  <img src={logout} width="26px" height="27px" alt="logout" />
                </button>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </>
);

Header.propTypes = {

};

export default Header;
