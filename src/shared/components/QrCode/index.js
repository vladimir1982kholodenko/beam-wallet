import React, { useState } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import qr from 'Root/assets/images/qr.png';
import CopyText from 'Root/shared/components/CopyText';
import styles from './styles.less';

const QrCode = (props) => {
  const [copy, changeCopy] = useState(false);
  return (
    <>
      <div className={classNames(styles.box, 'row mx-1')}>
        <div className="col-5 pl-0 position-relative">
          <div className={classNames(styles.square, 'position-relative')}>
            <img src={qr} alt="qr" className="child-center-hor-ver" />
          </div>
        </div>
        <div className="col-7 position-relative pr-5">
          <div className="child-center-ver pr-4">
            <p className={styles.code}>
            3b0353a850a437697b03fe80a8798d1e6f9f4916cb00b6fefead63401f1ad9868a4
            </p>
            <CopyText text="3b0353a850a437697b03fe80a8798d1e6f9f4916cb00b6fefead63401f1ad9868a4">
              <button
                type="button"
                className={styles.btn}
                onClick={() => {
                  changeCopy(!copy);
                  setTimeout(() => { changeCopy(false); }, 700);
                }}
              >
                <span className="icon-copy pr-2" />
                {copy && <span>Copied!</span>}
                {!copy && <span>Copy</span>}
              </button>
            </CopyText>
          </div>
        </div>
      </div>
    </>
  );
};

QrCode.propTypes = {

};

export default QrCode;
