import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import logo from 'Root/assets/images/logo.png';
import coin from 'Root/assets/images/coins.png';
import styles from './styles.less';

const TotalBeam = () => (
  <>
    <div className={classNames(styles.card, 'card card-primary')}>
      <div className="row h-100">
        <div className="col-6 pt-3 pb-2">
          <img src={logo} width="50px" height="34px" alt="beam" />
          <h3 className={styles.title}>121</h3>
          <p className={styles.text}>Beam</p>
        </div>
        <div className={classNames(styles.border, 'col-6 pt-3 pb-2')}>
          <img src={coin} width="30px" height="27px" alt="beam" />
          <h3 className={styles.title}><span>$</span>145</h3>
          <p className={styles.text}>USD</p>
        </div>
      </div>
    </div>
  </>
);

TotalBeam.propTypes = {

};

export default TotalBeam;
