### Quick Start

Install all packages and dependencies required for this project:

    npm install
    
Start the development environment (then, navigate to http://localhost:8080):

    npm run dev
  
Building files can be done as follows:

    npm run prod

